import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/*
 * GViewControl.java
 *
 * Version:
 * $Id: GViewControl.java,v 1.3 2013/04/24 01:39:22 zkf5289 Exp $
 * Revisions:
 * $Log: GViewControl.java,v $
 * Revision 1.3  2013/04/24 01:39:22  zkf5289
 * Final comments and formatting.
 *
 * Revision 1.2  2013/04/24 01:29:16  zkf5289
 * Now uses observerable and observer for updating the GUI.
 *
 * Revision 1.1  2013/04/24 00:20:20  zkf5289
 * Initial Push with most functionality.
 *
 */

/**
 * Class definition for the graphical view and controller.
 * 
 * @author Zachary Friss <ZKF5289@RIT.EDU>
 * 
 */

public class GViewControl extends JFrame implements Observer {

	private JTextField textField;

	private ConcentrationModel model;

	private Color colors[] = new Color[8];

	private ArrayList<CardButton> buttons = new ArrayList<CardButton>();

	private String status = "Select the first card.";

	/*
	 * Construct a GViewControl object.
	 * 
	 * @param model - The model for the view and controller.
	 */
	public GViewControl(ConcentrationModel concentrationModel) {

		this.model = concentrationModel;
		model.addObserver( this );

		colors[0] = Color.red;
		colors[1] = Color.orange;
		colors[2] = Color.yellow;
		colors[3] = Color.green;
		colors[4] = Color.blue;
		colors[5] = Color.pink;
		colors[6] = Color.CYAN;
		colors[7] = Color.white;

		setLayout( new BorderLayout() );

		JPanel panel1 = new JPanel();

		panel1.setLayout( new GridLayout( model.BOARD_SIZE, model.BOARD_SIZE ) );

		ButtonListener listener = new ButtonListener();

		for ( int i = 0; i < model.NUM_CARDS; i++ ) {

			CardButton button = new CardButton( i );
			button.setForeground( Color.BLACK );
			button.setBackground( Color.gray );
			button.setBorderPainted( true );
			button.setContentAreaFilled( false );
			button.setOpaque( true );
			button.setFocusPainted( false );
			button.addActionListener( listener );
			panel1.add( button );
			buttons.add( button );

		}

		JPanel mainpanel = new JPanel( new BorderLayout() );
		textField = new JTextField();
		textField.setForeground( Color.BLACK );
		textField.setText( "Moves :" + model.getMoveCount() + "   " + status );
		mainpanel.add( textField, BorderLayout.NORTH );
		mainpanel.add( panel1, BorderLayout.CENTER );

		add( mainpanel, BorderLayout.CENTER );

		JPanel panel3 = new JPanel( new FlowLayout() );
		JButton button = new JButton( "Reset" );
		button.setForeground( Color.RED );
		button.addActionListener( listener );
		panel3.add( button );

		button = new JButton( "Cheat" );
		button.setForeground( Color.RED );
		button.addActionListener( listener );
		panel3.add( button );

		button = new JButton( "Undo" );
		button.setForeground( Color.RED );
		button.addActionListener( listener );
		panel3.add( button );

		mainpanel.add( panel3, BorderLayout.SOUTH );

		setTitle( "Concentration Game" );
		setSize( 500, 500 );
		setLocation( 100, 100 );
		setVisible( true );
		setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );

	}

	/*
	 * ButtonListener handles button presses in the GUI and calls the correct
	 * model method.
	 */
	class ButtonListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {

			Object button = e.getSource();
			if ( button instanceof CardButton ) {

				int num = ((CardButton) button).getPos();
				model.selectCard( num );

			} else {

				if ( e.getActionCommand().compareTo( "Cheat" ) == 0 ) {
					ArrayList<CardButton> cheatbuttons = new ArrayList<CardButton>();
					ArrayList<CardFace> faces = model.cheat();
					for ( CardFace card : faces ) {
						CardButton cheatbutton = new CardButton( 0 );
						cheatbutton.setText( Integer.toString( (card
								.getNumber()) ) );
						cheatbutton.setForeground( Color.BLACK );
						cheatbutton.setBackground( colors[card.getNumber()] );
						cheatbutton.setBorderPainted( true );
						cheatbutton.setContentAreaFilled( false );
						cheatbutton.setOpaque( true );
						cheatbutton.setFocusPainted( false );
						cheatbuttons.add( cheatbutton );
					}
					CheatFrame cheater = new CheatFrame( cheatbuttons, buttons
							.get( 0 ).getSize().width * model.BOARD_SIZE );
				} else if ( e.getActionCommand().compareTo( "Reset" ) == 0 ) {
					model.reset();

				} else if ( e.getActionCommand().compareTo( "Undo" ) == 0 ) {
					model.undo();

				}
			}
		}
	}

	/**
	 * The main method used to play a game.
	 * 
	 * @param args
	 *            - unused
	 */
	public static void main(String[] args) {

		GViewControl game = new GViewControl( new ConcentrationModel() );

	}

	/*
	 * Update the window when the model indicates an update is required. Update
	 * changes the color and string content of a CardButton based on the
	 * CardFaces in the model, and it changes the text in the label based on the
	 * model state.
	 * 
	 * @param - arg0 - An Observable -- not used. arg1 - An Object -- not used.
	 */
	@Override
	public void update(Observable arg0, Object arg1) {

		ArrayList<CardFace> cards = model.getCards();

		for ( int i = 0; i < cards.size(); i++ ) {
			CardFace card = cards.get( i );
			if ( card.isFaceUp() ) {
				buttons.get( i ).setForeground( Color.BLACK );
				buttons.get( i ).setBackground( colors[card.getNumber()] );
				buttons.get( i ).setBorderPainted( true );
				buttons.get( i ).setContentAreaFilled( false );
				buttons.get( i ).setOpaque( true );
				buttons.get( i )
						.setText( Integer.toString( (card.getNumber()) ) );
			} else {
				buttons.get( i ).setForeground( Color.gray );
				buttons.get( i ).setBackground( Color.gray );
				buttons.get( i ).setBorderPainted( true );
				buttons.get( i ).setContentAreaFilled( false );
				buttons.get( i ).setOpaque( true );
			}
		}

		int up = model.howManyCardsUp();
		switch ( up ) {
		case 0:
			status = "Select the first card.";
			break;
		case 1:
			status = "Select the second card.";
			break;
		case 2:
			status = "No Match: Undo or select a card.";
			break;
		}

		textField.setText( "Moves :" + model.getMoveCount() + "   " + status );
		validate();

	}

}
