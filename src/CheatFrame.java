import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;




/*
 * CheatFrame.java
 *
 * Version:
 * $Id: CheatFrame.java,v 1.1 2013/04/24 00:20:20 zkf5289 Exp $
 * Revisions:
 * $Log: CheatFrame.java,v $
 * Revision 1.1  2013/04/24 00:20:20  zkf5289
 * Initial Push with most functionality.
 *
 */

/**
 * @author Friss
 *
 */

public class CheatFrame extends JFrame {

	 
	

	/*
	 * Construct a CheatFrame object.
	 * 
	 * @param 
	 * 		cardButtons - An ArrayList of CardButtons that are all showing their numbers.
	 *		size - The size (of one side) of the board (measured in cards).
	 */
	public CheatFrame(ArrayList<CardButton> cardButtons, int size){
		double framesize = size * .75;
		int frame = (int)framesize;
		
		 
		// Set the border layout for the frame
        setLayout(new BorderLayout());

        // Create panel1 for the button and
        // use a grid layout
        JPanel panel1 = new JPanel();
        
        panel1.setLayout(new GridLayout(4, 4));

       
      

        // Add buttons to the panel
        for (CardButton button : cardButtons) {
            panel1.add( button );
        }
        JPanel mainpanel = new JPanel(new BorderLayout());
        mainpanel.add( panel1, BorderLayout.CENTER );
        add(mainpanel, BorderLayout.CENTER);
        setTitle("Cheat Concentration Game");
        setSize(frame, frame);
        setLocation(200, 200);
        setVisible(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); 
		
		
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// TODO Auto-generated method stub

	}

}
